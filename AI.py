import ktrain
from ktrain import text
import tensorflow as tf
from pathlib import Path
import dataManagement
from sklearn import metrics


x_train, y_train, x_test, y_test = dataManagement.load()

model_name = 'roberta-base'
lr = 1e-5
wd = 0.01
log_path = 'logs/'+model_name+'/lr_'+str(lr)+'/wd_'+str(wd)
chk_path = 'models/'+ model_name+'-lr_'+str(lr)+'-wd_'+str(wd)

Path(log_path).mkdir(parents=True, exist_ok=True)

t = text.Transformer(model_name, maxlen=455)

trn = t.preprocess_train(x_train, y_train)
val = t.preprocess_test(x_test, y_test)

model = t.get_classifier(multilabel=False, metrics=[
    'root_mean_squared_error',
    'logcosh',
    'mean_absolute_error',
    'cosine_similarity'
    ])

learner = ktrain.get_learner(model, train_data=trn, val_data=val, batch_size=5)


class validate(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        p = ktrain.get_predictor(learner.model, preproc=t)
        y_pred = p.predict(x_test)#.tolist()
        f = open(log_path+'/e'+str(epoch+1)+'.txt', "a")
        f.writelines([
            "explained variance error: " +str(metrics.explained_variance_score(y_test, y_pred)), 
            "\nmax error: " + str(metrics.max_error(y_test, y_pred)),
            "\nmean absolute error: " + str(metrics.mean_absolute_error(y_test, y_pred)),
            "\nmean squared error: " + str(metrics.mean_squared_error(y_test, y_pred)),
            "\nrmse: " + str(metrics.mean_squared_error(y_test, y_pred, squared=False)),
            #"mean squared log error: " + str(metrics.mean_squared_log_error(y_test, y_pred)),
            "\nmedian absolute error: " + str(metrics.median_absolute_error(y_test, y_pred)),
            "\nr2 score: " + str(metrics.r2_score(y_test, y_pred)),
            #"mean poisson deviance: " + str(metrics.mean_poisson_deviance(y_test, y_pred)),
            #"mean gamma deviance: " + str(metrics.mean_gamma_deviance(y_test, y_pred)),
            #"mean absolute percentage error" + str(metrics.mean_absolute_percentage_error(y_test, y_pred))
            ])
        f.close()

        

learner.set_weight_decay(wd)

# learner.lr_find()
# learner.lr_plot()
# print(learner.lr_estimate())

learner.fit_onecycle(
    lr, 
    4, 
    checkpoint_folder='models/checkpoints/'+ model_name + '-lr_'  + str(lr) + '-wd_' + str(wd),
    callbacks=[
        tf.keras.callbacks.EarlyStopping(
                    patience=1,
                    monitor='val_loss',
                    mode='min',
                    restore_best_weights=True
                    ),
        validate()
    ])


predictor = ktrain.get_predictor(learner.model, preproc=t)

predictor.save(chk_path)