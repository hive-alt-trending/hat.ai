# Hive-Alt-Trending-AI

Data collection tools and model used for hive alternate trending page.

## Requirements
### Hardware
- >16GB rAM
- >8GB GPU MEMORY
### Softrare
- python 3.8
- tensorflow
- ktrain
- beem
- mistune
- bs4
- lxml
- marko
- modin
- langdetect
- sklearn

## Useage
- Run hive_data_scraper.py until all data has been downloaded
- Make sure all the json files you want to use are in the root of the data folder. By default, niche accounts are excluded.
- Run dataManagement.py as __main__ to preprocess the collected data.
- Run AI.py to train the model. (Minimum 8GB GPU memory required, estimated training time is 40 hours with a GTX 1080)
- model can be imported into code using ktrain.load_predictor()