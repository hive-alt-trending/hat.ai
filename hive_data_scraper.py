from beem.account import Account
from beem.comment import Comment
from beem.vote import Vote
from beem.nodelist import NodeList
from beem.exceptions import ContentDoesNotExistsException
from beem.exceptions import AccountDoesNotExistsException
from beem import Hive
from pathlib import Path
import json
from beem.instance import set_shared_blockchain_instance

nodelist = NodeList()
nodelist.update_nodes()
nodes = nodelist.get_hive_nodes()
hive = Hive(node=nodes)
set_shared_blockchain_instance(hive)


def scrape_data(accounts_txt, vote_type):
    with open(f"{accounts_txt}") as f:
        accounts = [line.rstrip() for line in f]
    # curated_accounts = ['curie']
    for account_name in accounts:
        try:
            account = Account(account_name)
            all_votes = {}

            for vote in account.history(only_ops=["vote"]):
                try:
                    vote = Vote(vote)

                    if vote['author'] == account_name:
                        continue

                    author = vote['author']
                    permlink = vote['permlink']
                    authorperm = f'@{author}/{permlink}'
                    weight = vote['weight']

                    if weight < 0 and vote_type == 'upvotes':
                        continue
                    if weight > 0 and vote_type == 'downvotes':
                        continue

                    c = Comment(authorperm)
                    # print(c.json())

                    if c.is_comment():
                        continue

                    v_data = {}
                    v_data['weight'] = weight
                    v_data['score'] = 0.5 + (weight / 10000) / 2
                    print(v_data, authorperm)
                    v_data['body'] = c['body']
                    all_votes[authorperm] = v_data
                except ContentDoesNotExistsException:
                    continue

        except AccountDoesNotExistsException:
            print(f"The '{account_name}' account does not exist. Please verify the list of accounts.")
            continue
            
        path = f"data/{account_name}-votes.json"
        if vote_type in ['niche']:
            Path(f'data/{vote_type}').mkdir(parents=True, exist_ok=True)
            path = f"data/{vote_type}/{account_name}-votes.json"
            
        with open(path, "w") as json_file:
            json.dump(all_votes, json_file, indent=4)

for list_of_accounts in [['upvotes', 'curated_accounts.txt'], ['downvotes', 'spam_accounts.txt'], ['niche', 'niche_accounts.txt']]:
    scrape_data(list_of_accounts[1], list_of_accounts[0])
    