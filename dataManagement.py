def detect_en(text):
    try:
        return detect(text) == 'en'
    except:
        return False
    
def remove_small_votes(weight):
    return weight > 100 or weight < -100

def strip_markdown_html(markup):
    try:
        md = mistune.html(markup)
    except:
        fails['mistune'] += 1
        try:
            md = markdown.markdown(markup)
        except:
            fails['markdown'] += 1
            try:
                md = marko.convert(markup)
            except:
                fails['marko'] += 1
                md = markup
                
    markup = BeautifulSoup(md, 'lxml')
    return markup.get_text()

def preprocess():
    datasets = list()
    os.chdir('./data')
    for file in glob.glob("*.json"):
        data = pd.read_json(file)
        data = data.transpose()
        #data = data[data['weight'].apply(remove_small_votes)]
        del data['score']
        data = data[data['body'].apply(detect_en)]
        data['body'] = data['body'].apply(strip_markdown_html)
        print(fails)
        datasets.append(data)
        
    combined = pd.concat(datasets)
    unique = combined[~combined.index.duplicated(keep='first')]
    Path('./preproc').mkdir(parents=True, exist_ok=True)
    unique.to_json('./preproc/combined.json')
    
def load(test_size=0.05):
    data = pd.read_json('data/preproc/combined.json')
    
    print(data)
    X_train, X_test, y_train, y_test = train_test_split(
                data.body.values, 
                data.weight.values, 
                test_size=test_size,
                random_state=42
                )
            
    train_b, test_b = Bunch(data=X_train, target=y_train), Bunch(data=X_test, target=y_test)
    
    return train_b.data, train_b.target, test_b.data, test_b.target
    
        
if __name__ == "__main__":
    import glob, os
    import modin.pandas as pd
    from langdetect import detect
    from bs4 import BeautifulSoup
    import marko, markdown, mistune
    from pathlib import Path
    fails = {'mistune': 0, 'markdown': 0, 'marko': 0}
    preprocess()
else:
    import modin.pandas as pd
    from sklearn.model_selection import train_test_split
    from sklearn.utils import Bunch